package oop.asg02;


public class BigInteger {

	public String value;

	public BigInteger() {
		value = "";
	}

	public BigInteger(String init) {
		value = strdel0(init);
	}
	
	public BigInteger(int init) {
		value = init + "";
	}
	
	public String toString() {
		return value;

	}

	public boolean equals(BigInteger big) {
		if (value.equals(big.value)) {
			return true;
		}
		return false;
	}

	public long toLong() {
		long big = Long.parseLong(value);
		return big;
	}

	public BigInteger add(BigInteger big) {
		String sum = "0";	// tao chuoi luu gia tri sau khi cong
		int leng , leng1, leng2; // 
		leng = 0;
		leng1 = value.length();	// luu do dai cua chuoi so hien tai
		leng2 = big.value.length();	// luu do dai cua chuoi so cong them vao
		String str, str1 , str2, str3;	
		str = str1 = str2 = str3 = "";
		/*
		 * tu tuong : tach chuoi so lon thanh 2 chuoi nho str va str1. trong do str1 se bang do dai chuoi so nho str2
		 * cong 2 chuoi str2 va str1 thanh chuoi str3
		 * sau do ghep str va str3
		 */
		
		
		if(leng2 > leng1){
			str = big.value.substring(0, leng2 - leng1);	// chia chuoi so lon thanh 2 str va str1
			str1 = big.value.substring(leng2 - leng1, leng2);
			str2 = value;	// sao luu chuoi so nho de xu ly
			leng = leng1;
		}
		if(leng2 < leng1){
			str = value.substring(0, leng1 - leng2);
			str1 = value.substring(leng1 - leng2, leng1);
			str2 = big.value;
			leng = leng2;
		}
		if(leng2 == leng1){	// truong hop bang nhau thi dat str1 la chuoi so hien tai va str2 la chuoi so cong vao
			str = "";
			str1 = value;
			str2 = big.value;
			leng = leng1;
		}
		
		int memory = 0; // tao bien nho de cong them 1
		for(int i = 1; i <= leng; i++){ 
			int x1 = Integer.parseInt(str1.charAt(leng - i) + "");	// lay ki tu thu i chuyen sang kieu so thuc de tinh toan
			int x2 = Integer.parseInt(str2.charAt(leng - i) + "");
			int x3 = x1 + x2 + memory;	// cong lai va cong ca so nho'
			if(x3 >= 10){	// neu tong > 10 thi so nho = 1 va chia lay du cho 10
				x3 = x3 % 10;
				memory = 1;
			}else{
				memory = 0;
			}
			str3 = x3 + str3;	// them so x3 vao dau. nhu vay so se tang dan
		}
		
		if(memory == 1){ // neu sau khi cong 2 chuoi bang nhau ma memory = 1 thi ta cong vao so cuoi cung cua chuoi str 1 don vi
			String temp = str.charAt(str.length() - 1) + "";
			int x = Integer.parseInt(temp);
			x++;
			str = str.substring(0, str.length() - 1) + x;
		}
		sum = str + str3;	// ghÄ‚Â©p lĂ¡ÂºÂ¡i dc kĂ¡ÂºÂ¿t quĂ¡ÂºÂ£
		
		return new BigInteger(sum);
	}
	
	public BigInteger clone() {
		return new BigInteger(value);
	}

	public String strdel0(String str) {
		int dem = 0;
		for(int i = 0; i < str.length(); i++){
			if(str.charAt(i) == '0'){
				dem++;
			}else
				break;
		}
		if(dem == str.length()){
			return "0";
		}
		return str.substring(dem, str.length()); 
	}

	public BigInteger subtract(BigInteger big) {
		String sub = "0";	// tao chuoi lu gia tri sau khi cong
		int leng , leng1, leng2; // 
		leng = 0;
		leng1 = value.length();	// luu do dai cua chuoi so hien tai
		leng2 = big.value.length();	// luu do dai cua chuoi so cong them vao
		String str, str1 , str2, str3;	
		str = str1 = str2 = str3 = "";
		int memory = 0; // tao bien nho de cong them 1
		/*
		 * tu tuong : tach chuoi so lon thanh 2 chuoi nho str va str1. trong do str1 se bang do dai chuoi so nho str2
		 * cong 2 chuoi str2 va str1 thanh chuoi str3
		 * sau do ghep str va str3 la ok 
		 */
		
		if(leng2 > leng1){
			str = big.value.substring(0, leng2 - leng1);	// chia chuoi so lon thanh 2 str va str1
			str1 = big.value.substring(leng2 - leng1, leng2);
			str2 = value;	// sao luu chuoi so nho de xu ly
			leng = leng1;
		}
		if(leng2 < leng1){
			str = value.substring(0, leng1 - leng2);
			str1 = value.substring(leng1 - leng2, leng1);
			str2 = big.value;
			leng = leng2;
		}
		if(leng2 == leng1){	// truong hop bang nhau thi dat str1 la chuoi so hien tai va str2 la chuoi so cong vao
			str = "";
			str1 = value;
			str2 = big.value;
			leng = leng1;
		}
		int x3 = 0;
		for(int i = 1; i <= leng; i++){ 
			int x1 = Integer.parseInt(str1.charAt(leng - i) + "");	// lay ki tu thu i chuyen sang kieu so thuc de tinh toan
			int x2 = Integer.parseInt(str2.charAt(leng - i) + "") + memory;
			if(x1 >= x2){
				memory = 0;
				x3 = x1 - x2;
			}else{
				memory = 1;
				x3 = x1 + 10 - x2;
			}
			str3 = x3 + str3;	// them so x3 vao dau. nhu vay so se tang dan
		}
		
		if(memory == 1){ // neu sau khi cong 2 chuoi bang nhau ma memory = 1 thi ta cong vao so cuoi cung cua chuoi str 1 don vi
			String temp = str.charAt(str.length() - 1) + "";
			int x = Integer.parseInt(temp);
			x--;
			str = str.substring(0, str.length() - 1) + x;
		}
		sub = str + str3;	
		int[] subInt = convertStringToInt(sub);
		String result = convertIntToString(subInt);
		return new BigInteger(result);
	}
	
	public int[] convertStringToInt(String input){
		
		int[] result = new int[input.length()];
		
		for (int i=0 ; i<input.length() ; i++){
			result[i] = Integer.parseInt(input.charAt(i) + "");
		}
		
		return result;
	}

	public String convertIntToString(int[] input){
	
		String result = "";
		for (int i=0 ; i<input.length ; i++){
			result += input[i];
		}
		
		return result;
	}
	
	
	public static void main(String[] args) {
		BigInteger bigInt = new BigInteger("0000000");
		System.out.println(bigInt.toString());
		BigInteger bigInt1 = new BigInteger("111111111111111111111");
        BigInteger bigInt2 = new BigInteger("1");
        BigInteger difference = bigInt1.subtract(bigInt2);
        System.out.println(difference.toString());
		
		
	}
}



