package Main;

public class BigInteger {

	public String value;

	public BigInteger() {
		value = "0";
	}

	public BigInteger(int init) {
		value = String.valueOf(init);
	}

	public BigInteger(String init) {
		value = strdel0(init);
	}

	public String toString() {
		return value;

	}

	public boolean equals(BigInteger big) {
		if (value.equals(big.value)) {
			return true;
		}
		return false;
	}

	public long toLong() {
		long big = Long.parseLong(value);
		return big;
	}

	public BigInteger add(BigInteger big) {
		String sum = "0";	
		int leng , leng1, leng2;  
		leng = 0;
		leng1 = value.length();	
		leng2 = big.value.length();	
		String str, str1 , str2, str3;	
		str = str1 = str2 = str3 = "";
		
		
		
		if(leng2 > leng1){
			str = big.value.substring(0, leng2 - leng1);	
			str1 = big.value.substring(leng2 - leng1, leng2);
			str2 = value;	
			leng = leng1;
		}
		if(leng2 < leng1){
			str = value.substring(0, leng1 - leng2);
			str1 = value.substring(leng1 - leng2, leng1);
			str2 = big.value;
			leng = leng2;
		}
		if(leng2 == leng1){	
			str = "";
			str1 = value;
			str2 = big.value;
			leng = leng1;
		}
		
		int memory = 0; 
		for(int i = 1; i <= leng; i++){ 
			int x1 = Integer.parseInt(str1.charAt(leng - i) + "");	
			int x2 = Integer.parseInt(str2.charAt(leng - i) + "");
			int x3 = x1 + x2 + memory;	
			if(x3 >= 10){	
				x3 = x3 % 10;
				memory = 1;
			}else{
				memory = 0;
			}
			str3 = x3 + str3;	
		}
		
		if(memory == 1){
			String temp = str.charAt(str.length() - 1) + "";
			int x = Integer.parseInt(temp);
			x++;
			str = str.substring(0, str.length() - 1) + x;
		}
		sum = str + str3;	
		
		return new BigInteger(sum);
	}
	

	public String strdel0(String str) {
		while (true) {
			int leng = str.length();
			if (str.charAt(0) == '0') {
				str = str.substring(1, leng);
			} else
				return str;
		}
	}

	public BigInteger subtract(BigInteger big) {
		String sub = "0";	
		int leng , leng1, leng2; 
		leng = 0;
		leng1 = value.length();	
		leng2 = big.value.length();	
		String str, str1 , str2, str3;	
		str = str1 = str2 = str3 = "";
		int memory = 0; 
		
		
		if(leng2 > leng1){
			str = big.value.substring(0, leng2 - leng1);	
			str1 = big.value.substring(leng2 - leng1, leng2);
			str2 = value;	
			leng = leng1;
		}
		if(leng2 < leng1){
			str = value.substring(0, leng1 - leng2);
			str1 = value.substring(leng1 - leng2, leng1);
			str2 = big.value;
			leng = leng2;
		}
		if(leng2 == leng1){	
			str = "";
			str1 = value;
			str2 = big.value;
			leng = leng1;
		}
		int x3 = 0;
		for(int i = 1; i <= leng; i++){ 
			int x1 = Integer.parseInt(str1.charAt(leng - i) + "");	
			int x2 = Integer.parseInt(str2.charAt(leng - i) + "") + memory;
			if(x1 >= x2){
				memory = 0;
				x3 = x1 - x2;
			}else{
				memory = 1;
				x3 = x1 + 10 - x2;
			}
			str3 = x3 + str3;	
		}
		
		if(memory == 1){ 
			String temp = str.charAt(str.length() - 1) + "";
			int x = Integer.parseInt(temp);
			x--;
			str = str.substring(0, str.length() - 1) + x;
		}
		sub = str + str3;	
		
		return new BigInteger(sub);
	}
	

	public static void main(String[] args) {
		BigInteger big1 = new BigInteger("1234567890987654321");
		BigInteger big2 = new BigInteger("000000987654321123456789");
		BigInteger big3 = new BigInteger("1238346588745162856812");
		BigInteger big4 = new BigInteger("000000373487583475873");

		System.out.println(big1.toString());
		System.out.println(big2.toString());
		
		System.out.println(big1.add(big2).toString());
		System.out.println(big3.subtract(big4).toString());
		

	}
}

